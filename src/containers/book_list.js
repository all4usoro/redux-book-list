import React, { Component} from "react"
import { connect } from 'react-redux'
import { selectBook } from "../actions/index"
import { bindActionCreators } from "redux"


class BookList extends Component {
  renderList() {
    return this.props.books.map((book) => {
      return (
        <li
        onClick={() => this.props.selectBook(book)}
        key={book.title}
        className="list-group-item">{book.title} </li>
      )
    })
  }
  render() {
    return(
      <ul className="list-group col-sm-4">
        { this.renderList() }
      </ul>
    )
  }
}

/**
  The purpose of this function is to take our application state
   as an argument
   Note: state contains the array of books and active books
**/
function mapStateToProps(state) {
  //Whatever gets returned from here will show up as props
  //inside of BookList
  return {
     //asdf: '123' //Will access this.props.asdf
     books : state.books
  }
}

//The dispatch says "I'll make sure the actions get passed to all the reducers in the application"
function mapDispatchToProps(dispatch) {
  //Whenever select book is called
  //The result should be passed to all our reducers
  return bindActionCreators({
    selectBook: selectBook
  }, dispatch)
}
//Whatever is returned will end up as props (this.props.selectBook)
//And this will call selectBook() action creator

export default connect(mapStateToProps, mapDispatchToProps)(BookList);
