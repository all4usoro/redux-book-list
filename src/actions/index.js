export const BOOK_SELECTED = "BOOK_SELECTED"

export function selectBook(book) {
  console.log("Book has been selected")
  return {
    type: BOOK_SELECTED,
    payload: book
   }
}
